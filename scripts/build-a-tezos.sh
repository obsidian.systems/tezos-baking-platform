#! /usr/bin/env bash

set -e -o pipefail

root="$(git rev-parse --show-toplevel)"

usage () {
    cat >&2 <<EOF
output=<PATH> [org=<ORG>] [branch=BRANCH] [opam_switch=SWITCHNAME] ./scripts/build-a-tezos.sh

PATH: an empty directory
ORG: tezos or obsidian.systems (= default)
BRANCH: git-branch (default = master)
SWITCHNAME: use an existing opam-switch instead of making a new one

Example:

     output=/tmp/zerotezos org=tezos branch=zeronet ./scripts/build-a-tezos.sh

EOF
}
if [ "$1" != "" ] ; then
    usage
    exit 2
fi


nix_script="$(mktemp -t build-tezos-XXXXX.bash)"

tezos_branch="${branch:-master}"
tezos_org="${org:-obsidian.systems}"

if [ "$opam_switch" = "" ] ; then
    ensure_opam_switch="make build-dev-deps"
else
    ensure_opam_switch="export OPAMSWITCH=$opam_switch"
fi

output_dir="$output"
if [ "$output_dir" = "" ] ; then
    echo "At least \$output should be provided"
    exit 3
fi

cat > $nix_script <<EOF
set -e

echo "Opam: \$(which opam) -> \$(opam --version)"
echo "Tezos remote: $tezos_org"
echo "Tezos branch: $tezos_branch"

mkdir -p "$output_dir"
git clone https://gitlab.com/$tezos_org/tezos.git -b $tezos_branch $output_dir
cd "$output_dir"

$ensure_opam_switch

eval \$(opam env)

make all build-test

EOF

if [ "$just_script" = "true" ] ; then
    cat $nix_script
else
    nix-shell "$root" -A tezos.master.opam-box --run "bash $nix_script"
fi




