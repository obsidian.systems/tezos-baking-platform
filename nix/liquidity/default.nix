{ lib, stdenv, fetchThunkWithName, ocaml, bigstring, calendar, digestif, easy-format, ezjsonm, hex, lwt, lwt_log, menhir, findlib, ocp-build, ocplib-endian, ocurl, sodium, uri, zarith }:

stdenv.mkDerivation rec {
  name = "liquidity-${version}";
  version = "1.03";
  srcs = [ (fetchThunkWithName "liquidity" ../../liquidity)
           (fetchThunkWithName "tezos" ../../tezos/for-liquidity) ];
  sourceRoot = "liquidity";
  preUnpack = "export OCP_HOME=\"$(pwd)\/.ocp\"";
  postUnpack = ''
    rm -rf liquidity/tezos
    chmod -R u+w tezos
    ln -s ../tezos liquidity/tezos
  '';
  # postUnpack = "rm -rf liquidity/tezos; chmod -R u+w tezos; mv tezos liquidity/";
  preBuild = "export OPAM_PREFIX=$out";
  buildInputs = [ ocaml bigstring calendar digestif easy-format ezjsonm hex lwt lwt_log menhir findlib ocp-build ocplib-endian ocurl sodium uri zarith ];

  meta = {
    homepage = https://www.liquidity-lang.org/;
    description = "Liquidity contract language compiler and tools";
    license = stdenv.lib.licenses.gpl3;
    inherit (ocaml.meta) platforms;
  };
}
