/*opam-version: "2.0"
  name: "conf-libcurl"
  version: "1"
  synopsis: "Virtual package relying on a libcurl system
  installation"
  description:
    "This package can only install if the libcurl is installed on the
  system."
  maintainer: "blue-prawn"
  authors: "Daniel Stenberg"
  license: "BSD-like"
  homepage: "http://curl.haxx.se/"
  bug-reports: "https://github.com/ocaml/opam-repository/issues"
  depends: [
    "conf-pkg-config" {build & os != "macos"}
  ]
  flags: conf
  build: ["pkg-config" "libcurl"] {os != "macos"}
  depexts: [
    ["libcurl4-gnutls-dev"] {os-distribution = "debian"}
    ["libcurl-devel"] {os-distribution = "mageia"}
    ["libcurl4-gnutls-dev"] {os-distribution = "ubuntu"}
    ["libcurl-devel" "openssl-devel"] {os-distribution = "centos"}
    ["curl"] {os-distribution = "nixos"}
    ["curl"] {os-distribution = "arch"}
    ["curl-dev"] {os-distribution = "alpine"}
    ["libcurl-devel"] {os-family = "suse"}
    ["libcurl-devel"] {os-distribution = "fedora"}
  ]*/
{ runCommand, doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl,
  conf-pkg-config, findlib, curl }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in

stdenv.mkDerivation rec {
  pname = "conf-libcurl";
  version = "1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    conf-pkg-config findlib curl ];
  propagatedBuildInputs = [
    conf-pkg-config curl ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'pkg-config'" "'libcurl'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
