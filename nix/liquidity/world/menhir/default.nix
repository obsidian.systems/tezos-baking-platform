/*opam-version: "2.0"
  name: "menhir"
  version: "20181113"
  synopsis: "An LR(1) parser generator"
  maintainer: "francois.pottier@inria.fr"
  authors: [
    "François Pottier <francois.pottier@inria.fr>"
    "Yann Régis-Gianas <yrg@pps.univ-paris-diderot.fr>"
  ]
  homepage: "http://gitlab.inria.fr/fpottier/menhir"
  bug-reports: "menhir@inria.fr"
  depends: [
    "ocaml" {>= "4.02"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
  ]
  build: [
    make
    "-f"
    "Makefile"
    "PREFIX=%{prefix}%"
    "USE_OCAMLFIND=true"
    "docdir=%{doc}%/menhir"
    "libdir=%{lib}%/menhir"
    "mandir=%{man}%/man1"
  ]
  install: [
    make
    "-f"
    "Makefile"
    "install"
    "PREFIX=%{prefix}%"
    "docdir=%{doc}%/menhir"
    "libdir=%{lib}%/menhir"
    "mandir=%{man}%/man1"
  ]
  remove: [
    make
    "-f"
    "Makefile"
    "uninstall"
    "PREFIX=%{prefix}%"
    "docdir=%{doc}%/menhir"
    "libdir=%{lib}%/menhir"
    "mandir=%{man}%/man1"
  ]
  dev-repo: "git+https://gitlab.inria.fr/fpottier/menhir.git"
  url {
    src:
     
  "https://gitlab.inria.fr/fpottier/menhir/repository/20181113/archive.tar.gz"
    checksum: [
      "md5=69ce441a06ea131cd43e7b44c4303f3c"
     
  "sha512=4ddefcd71d305bfb933a4056da57e36c13c99ec6dfcc4695814798fbbd78b4d65828381ebcb0e58c4c0394105ac763af3d475474e05e408f7080315bc3cf6176"
    ]
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  findlib, ocamlbuild }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.02") >= 0;

stdenv.mkDerivation rec {
  pname = "menhir";
  version = "20181113";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://gitlab.inria.fr/fpottier/menhir/repository/20181113/archive.tar.gz";
    sha256 = "0hl611l0gyl7b2bm7m0sk7vjz14m0i7znrnjq3gw58pylj934dx4";
  };
  buildInputs = [
    ocaml findlib ocamlbuild ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "make" "'-f'" "'Makefile'" "'PREFIX='$out" "'USE_OCAMLFIND=true'"
      "'docdir='$out/share/doc'/menhir'"
      "'libdir='$OCAMLFIND_DESTDIR'/menhir'" "'mandir='$out/man'/man1'" ]
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "make" "'-f'" "'Makefile'" "'install'" "'PREFIX='$out"
      "'docdir='$out/share/doc'/menhir'"
      "'libdir='$OCAMLFIND_DESTDIR'/menhir'" "'mandir='$out/man'/man1'" ]
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
