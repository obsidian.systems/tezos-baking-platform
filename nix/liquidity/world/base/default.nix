/*opam-version: "2.0"
  name: "base"
  version: "v0.12.0"
  synopsis: "Full standard library replacement for OCaml"
  description: """
  Full standard library replacement for OCaml
  
  Base is a complete and portable alternative to the OCaml standard
  library. It provides all standard functionalities one would expect
  from a language standard library. It uses consistent conventions
  across all of its module.
  
  Base aims to be usable in any context. As a result system dependent
  features such as I/O are not offered by Base. They are instead
  provided by companion libraries such as stdio:
  
    https://github.com/janestreet/stdio"""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/base"
  doc:
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/base/index.html"
  bug-reports: "https://github.com/janestreet/base/issues"
  depends: [
    "ocaml" {>= "4.04.2" & < "4.08.0"}
    "sexplib0" {>= "v0.12" & < "v0.13"}
    "dune" {build & >= "1.5.1"}
  ]
  depopts: ["base-native-int63"]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/base.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/base-v0.12.0.tar.gz"
    checksum: "md5=e522176bc2cca7c12745539fa72356ad"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  sexplib0, dune, findlib, base-native-int63 ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.04.2") >= 0 && (vcompare ocaml "4.08.0") < 0;
assert (vcompare sexplib0 "v0.12") >= 0 && (vcompare sexplib0 "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "base";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/base-v0.12.0.tar.gz";
    sha256 = "1vqfcvjg97kibf585a5c2d6hlwi8fnlw0ba2il01k3j6lfb78lv8";
  };
  buildInputs = [
    ocaml sexplib0 dune findlib ]
  ++
  stdenv.lib.optional
  (base-native-int63
  !=
  null)
  base-native-int63;
  propagatedBuildInputs = [
    ocaml sexplib0 dune ]
  ++
  stdenv.lib.optional
  (base-native-int63
  !=
  null)
  base-native-int63;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
