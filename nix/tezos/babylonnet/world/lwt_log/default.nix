/*opam-version: "2.0"
  name: "lwt_log"
  version: "1.1.1"
  synopsis: "Lwt logging library (deprecated)"
  maintainer: "Anton Bachin <antonbachin@yahoo.com>"
  authors: ["Shawn Wagner" "Jérémie Dimino"]
  license: "LGPL-2.0-or-later"
  homepage: "https://github.com/ocsigen/lwt_log"
  doc:
   
  "https://github.com/ocsigen/lwt_log/blob/master/src/core/lwt_log_core.mli"
  bug-reports: "https://github.com/ocsigen/lwt_log/issues"
  depends: [
    "ocaml"
    "dune" {>= "1.0"}
    "lwt" {>= "4.0.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocsigen/lwt_log.git"
  url {
    src: "https://github.com/aantron/lwt_log/archive/1.1.1.tar.gz"
    checksum: [
      "md5=02e93be62288037870ae5b1ce099fe59"
     
  "sha256=14fb19ec61e555e32c8bb026a591c0ce5b0a153663d17b0876178ab92d625f3f"
     
  "sha512=df3d171a7c72f37e96b756d252ab586767df9c13e01500faf13d4b2cee936b0602fd7c725c03db488d3737d8d92300af103d395a926dc654a2c44a5d6068f24a"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, lwt, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.1.1"; in
assert (vcompare dune "1.0") >= 0;
assert (vcompare lwt "4.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "lwt_log";
  version = "1.1.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/aantron/lwt_log/archive/1.1.1.tar.gz";
    sha256 = "0gszc8nvk2hpfq47plb36qahlnyfq28sa9mhicnf6mg5c7n1kyql";
  };
  buildInputs = [
    ocaml dune lwt findlib ];
  propagatedBuildInputs = [
    ocaml dune lwt ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
