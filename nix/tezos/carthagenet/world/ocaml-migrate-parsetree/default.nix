/*opam-version: "2.0"
  name: "ocaml-migrate-parsetree"
  version: "1.3.1"
  synopsis: "Convert OCaml parsetrees between different versions"
  description: """
  Convert OCaml parsetrees between different versions
  
  This library converts parsetrees, outcometree and ast mappers
  between
  different OCaml versions.  High-level functions help making PPX
  rewriters independent of a compiler version."""
  maintainer: "frederic.bour@lakaban.net"
  authors: [
    "Frédéric Bour <frederic.bour@lakaban.net>"
    "Jérémie Dimino <jeremie@dimino.org>"
  ]
  license: "LGPL-2.1-only with OCaml-LGPL-linking-exception"
  tags: ["syntax" "org:ocamllabs"]
  homepage: "https://github.com/ocaml-ppx/ocaml-migrate-parsetree"
  doc: "https://ocaml-ppx.github.io/ocaml-migrate-parsetree/"
  bug-reports:
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/issues"
  depends: [
    "result"
    "ppx_derivers"
    "dune" {>= "1.6.0"}
    "ocaml" {>= "4.02.3" & < "4.09.0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml-ppx/ocaml-migrate-parsetree.git"
  url {
    src:
     
  "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.3.1/ocaml-migrate-parsetree-v1.3.1.tbz"
    checksum: [
     
  "sha256=83e4955a6fd6b494646ab92c476840ea96b5fb434435c287e7ad3e6efadc8338"
     
  "sha512=7da86f9596dd1439990a6f087fdeba64a0f3ce2634473be4cca92ecc02b6fcd97917956890fbe35b3cba5a126d007afec6ede1e4afd0a5218c89fd6079ad4182"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml-result, ppx_derivers, dune, ocaml, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.3.1"; in
assert (vcompare dune "1.6.0") >= 0;
assert (vcompare ocaml "4.02.3") >= 0 && (vcompare ocaml "4.09.0") < 0;

stdenv.mkDerivation rec {
  pname = "ocaml-migrate-parsetree";
  version = "1.3.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ocaml-migrate-parsetree/releases/download/v1.3.1/ocaml-migrate-parsetree-v1.3.1.tbz";
    sha256 = "0f43vkx6wgmdwy3w4da48gxvb5pa81l4fb5rd9j99d6ndxd9br43";
  };
  buildInputs = [
    ocaml-result ppx_derivers dune ocaml findlib ];
  propagatedBuildInputs = [
    ocaml-result ppx_derivers dune ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
