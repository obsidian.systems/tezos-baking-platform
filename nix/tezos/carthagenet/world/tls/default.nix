/*opam-version: "2.0"
  name: "tls"
  version: "0.10.5"
  synopsis: "Transport Layer Security purely in OCaml"
  description: """
  Transport Layer Security (TLS) is probably the most widely deployed
  security
  protocol on the Internet. It provides communication privacy to
  prevent
  eavesdropping, tampering, and message forgery. Furthermore, it
  optionally
  provides authentication of the involved endpoints. TLS is commonly deployed
  for
  securing web services ([HTTPS](http://tools.ietf.org/html/rfc2818)),
  emails,
  virtual private networks, and wireless networks.
  
  TLS uses asymmetric cryptography to exchange a symmetric key, and
  optionally
  authenticate (using X.509) either or both endpoints. It provides
  algorithmic
  agility, which means that the key exchange method, symmetric
  encryption
  algorithm, and hash algorithm are negotiated.
  
  Read [further](https://nqsb.io) and our [Usenix Security 2015
  paper](https://usenix15.nqsb.io)."""
  maintainer: [
    "Hannes Mehnert <hannes@mehnert.org>" "David Kaloper
  <david@numm.org>"
  ]
  authors: [
    "David Kaloper <david@numm.org>" "Hannes Mehnert
  <hannes@mehnert.org>"
  ]
  license: "BSD2"
  tags: "org:mirage"
  homepage: "https://github.com/mirleft/ocaml-tls"
  doc: "https://mirleft.github.io/ocaml-tls/doc"
  bug-reports: "https://github.com/mirleft/ocaml-tls/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
    "ppx_sexp_conv" {< "v0.13"}
    "ppx_deriving"
    "ppx_cstruct" {>= "3.0.0"}
    "cstruct" {>= "4.0.0"}
    "cstruct-sexp"
    "sexplib" {< "v0.13"}
    "nocrypto" {>= "0.5.4"}
    "x509" {>= "0.7.0"}
    "domain-name" {>= "0.3.0"}
    "fmt"
    "cstruct-unix" {with-test & >= "3.0.0"}
    "ounit" {with-test}
  ]
  depopts: ["lwt" "mirage-flow" "mirage-kv" "mirage-clock" "ptime"]
  conflicts: [
    "lwt" {< "2.4.8"}
    "mirage-kv" {< "3.0.0"}
    "mirage-flow" {< "2.0.0"}
    "mirage-clock" {< "3.0.0"}
    "sexplib" {= "v0.9.0"}
    "ppx_sexp_conv" {= "v0.11.0"}
    "ptime" {< "0.8.1"}
  ]
  build: [
    [
      "ocaml"
      "pkg/pkg.ml"
      "build"
      "--pinned"
      "%{pinned}%"
      "--tests"
      "false"
      "--with-lwt"
      "%{lwt+ptime:installed}%"
      "--with-mirage"
      "%{mirage-flow+mirage-kv+mirage-clock+ptime:installed}%"
    ]
    [
      "ocaml"
      "pkg/pkg.ml"
      "build"
      "--pinned"
      "%{pinned}%"
      "--tests"
      "true"
      "--with-lwt"
      "%{lwt+ptime:installed}%"
      "--with-mirage"
      "%{mirage-flow+mirage-kv+mirage-clock+ptime:installed}%"
    ] {with-test}
    ["ocaml" "pkg/pkg.ml" "test"] {with-test}
  ]
  dev-repo: "git+https://github.com/mirleft/ocaml-tls.git"
  url {
    src:
     
  "https://github.com/mirleft/ocaml-tls/releases/download/v0.10.5/tls-0.10.5.tbz"
    checksum: [
      "md5=57d9477ea79080e9d2485007289cbc5f"
     
  "sha256=f61f95d62fb70579fe851ed60dbce1ff08e82d188daa999d9ea22b927e7245eb"
     
  "sha512=9884a96e50165a1809d5d27c5fdde78fb8a2a1135291fd607ea4400ad36c8664c7babd66dd5686320436aa03f9eff868f25f08b92676c0181d9e5f60c8ee1243"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, ppx_sexp_conv, ppx_deriving,
  ppx_cstruct, cstruct, cstruct-sexp, sexplib, nocrypto, x509, domain-name,
  fmt, cstruct-unix ? null, ounit ? null, lwt ? null, mirage-flow ? null,
  mirage-kv ? null, mirage-clock ? null, ptime ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.10.5"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare ppx_sexp_conv "v0.13") < 0;
assert (vcompare ppx_cstruct "3.0.0") >= 0;
assert (vcompare cstruct "4.0.0") >= 0;
assert (vcompare sexplib "v0.13") < 0;
assert (vcompare nocrypto "0.5.4") >= 0;
assert (vcompare x509 "0.7.0") >= 0;
assert (vcompare domain-name "0.3.0") >= 0;
assert doCheck -> (vcompare cstruct-unix "3.0.0") >= 0;
assert lwt != null -> !((vcompare lwt "2.4.8") < 0);
assert mirage-kv != null -> !((vcompare mirage-kv "3.0.0") < 0);
assert mirage-flow != null -> !((vcompare mirage-flow "2.0.0") < 0);
assert mirage-clock != null -> !((vcompare mirage-clock "3.0.0") < 0);
assert stdenv.lib.getVersion sexplib != "v0.9.0";
assert stdenv.lib.getVersion ppx_sexp_conv != "v0.11.0";
assert ptime != null -> !((vcompare ptime "0.8.1") < 0);

stdenv.mkDerivation rec {
  pname = "tls";
  version = "0.10.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirleft/ocaml-tls/releases/download/v0.10.5/tls-0.10.5.tbz";
    sha256 = "1ss5f9z94ax2ksfrkald30nyh27zw6y0vmhyhpz7j1dp5zb9a7zn";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ppx_sexp_conv ppx_deriving ppx_cstruct
    cstruct cstruct-sexp sexplib nocrypto x509 domain-name fmt cstruct-unix ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt
  ++
  stdenv.lib.optional
  (mirage-flow
  !=
  null)
  mirage-flow
  ++
  stdenv.lib.optional
  (mirage-kv
  !=
  null)
  mirage-kv
  ++
  stdenv.lib.optional
  (mirage-clock
  !=
  null)
  mirage-clock
  ++
  stdenv.lib.optional
  (ptime
  !=
  null)
  ptime;
  propagatedBuildInputs = [
    ocaml ppx_sexp_conv ppx_deriving ppx_cstruct cstruct cstruct-sexp sexplib
    nocrypto x509 domain-name fmt cstruct-unix ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  (lwt
  !=
  null)
  lwt
  ++
  stdenv.lib.optional
  (mirage-flow
  !=
  null)
  mirage-flow
  ++
  stdenv.lib.optional
  (mirage-kv
  !=
  null)
  mirage-kv
  ++
  stdenv.lib.optional
  (mirage-clock
  !=
  null)
  mirage-clock
  ++
  stdenv.lib.optional
  (ptime
  !=
  null)
  ptime;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false" "'--tests'"
      "'false'" "'--with-lwt'"
      "${if lwt != null && ptime != null then "true" else "false"}" "'--with-mirage'" "${if
                                                                    mirage-flow
                                                                    != null
                                                                    &&
                                                                    mirage-kv
                                                                    != null
                                                                    &&
                                                                    mirage-clock
                                                                    != null
                                                                    && ptime
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] (stdenv.lib.optionals doCheck [ "'ocaml'"
                                                                    "'pkg/pkg.ml'"
                                                                    "'build'"
                                                                    "'--pinned'"
                                                                    "false"
                                                                    "'--tests'"
                                                                    "'true'"
                                                                    "'--with-lwt'"
                                                                    "${if
                                                                    lwt !=
                                                                    null &&
                                                                    ptime !=
                                                                    null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" "'--with-mirage'" "${if
                                                                    mirage-flow
                                                                    != null
                                                                    &&
                                                                    mirage-kv
                                                                    != null
                                                                    &&
                                                                    mirage-clock
                                                                    != null
                                                                    && ptime
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ]) (stdenv.lib.optionals doCheck [ "'ocaml'"
                                                                    "'pkg/pkg.ml'"
                                                                    "'test'" ]) ];
                                                                    preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
                                                                    [ ];
                                                                    installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
                                                                    createFindlibDestdir = true; }
