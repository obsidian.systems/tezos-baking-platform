/*opam-version: "2.0"
  name: "eqaf"
  version: "0.5"
  synopsis: "Constant-time equal function on string"
  description:
    "This package provides an equal function on string in constant-time to
  avoid timing-attack with crypto stuff."
  maintainer: "Romain Calascibetta <romain.calascibetta@gmail.com>"
  authors: "Romain Calascibetta <romain.calascibetta@gmail.com>"
  license: "MIT"
  homepage: "https://github.com/mirage/eqaf"
  doc: "https://mirage.github.io/eqaf/"
  bug-reports: "https://github.com/mirage/eqaf/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune"
    "alcotest" {with-test}
    "crowbar" {with-test}
  ]
  depopts: [
    "cstruct" {>= "4.0.0"}
    "bigarray-compat"
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  run-test: ["dune" "runtest" "-p" name "-j" "1" "--no-buffer"
  "--verbose"]
  dev-repo: "git+https://github.com/mirage/eqaf.git"
  url {
    src:
  "https://github.com/mirage/eqaf/releases/download/v0.5/eqaf-v0.5.tbz"
    checksum: [
     
  "sha256=58ef81c110ee44669b1951df669cdc1f60ca34f0063cf0ae8b87568111af73f2"
     
  "sha512=8a5761596bb8cbde8743161502900423f369d3de3b4b999a940b93d66b1830204fdb67165eff461f3eec0cdb1e169ebd6c9b80f5a8162bc12f359b606a0cbb17"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, alcotest ? null, crowbar ? null, findlib,
  cstruct ? null, bigarray-compat ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.5"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert cstruct != null -> (vcompare cstruct "4.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "eqaf";
  version = "0.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/eqaf/releases/download/v0.5/eqaf-v0.5.tbz";
    sha256 = "1wkkmw8q2ml7ifpg0g06y0sclq0zvjf6dpsi36dnci7f230q3vsq";
  };
  buildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  stdenv.lib.optional
  doCheck
  crowbar
  ++
  [
    findlib ]
  ++
  stdenv.lib.optional
  (cstruct
  !=
  null)
  cstruct
  ++
  stdenv.lib.optional
  (bigarray-compat
  !=
  null)
  bigarray-compat;
  propagatedBuildInputs = [
    ocaml dune ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  stdenv.lib.optional
  doCheck
  crowbar
  ++
  stdenv.lib.optional
  (cstruct
  !=
  null)
  cstruct
  ++
  stdenv.lib.optional
  (bigarray-compat
  !=
  null)
  bigarray-compat;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
