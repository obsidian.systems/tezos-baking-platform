/*opam-version: "2.0"
  name: "ocaml-compiler-libs"
  version: "v0.12.1"
  synopsis: "OCaml compiler libraries repackaged"
  description: """
  This packages exposes the OCaml compiler libraries repackages under
  the toplevel names Ocaml_common, Ocaml_bytecomp, Ocaml_optcomp,
  ..."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "Apache-2.0"
  homepage: "https://github.com/janestreet/ocaml-compiler-libs"
  bug-reports:
  "https://github.com/janestreet/ocaml-compiler-libs/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {>= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/ocaml-compiler-libs.git"
  url {
    src:
     
  "https://github.com/janestreet/ocaml-compiler-libs/archive/v0.12.1.tar.gz"
    checksum: [
      "md5=2f929af7c764a3f681a5671f271210c4"
     
  "sha256=d20f217992f26fa2012c67537391d55e1619e37c51087d1d65b583f08c849a04"
     
  "sha512=f4358ab6a1be8405b077d690ca636cc0af9507fdc7836609e0ddc1f841488f24cb9741869f1ea95ca3dff82f25f60dcf907f4f7c049bdf282c1936f3fefd6808"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.1"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "ocaml-compiler-libs";
  version = "v0.12.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/janestreet/ocaml-compiler-libs/archive/v0.12.1.tar.gz";
    sha256 = "014shj6g10xmclfps22igkiij5jysn8p6lv75h0s4vzjj9wj23yj";
  };
  buildInputs = [
    ocaml dune findlib ];
  propagatedBuildInputs = [
    ocaml dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
