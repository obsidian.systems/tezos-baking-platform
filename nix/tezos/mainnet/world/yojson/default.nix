/*opam-version: "2.0"
  name: "yojson"
  version: "1.7.0"
  synopsis:
    "Yojson is an optimized parsing and printing library for the JSON
  format"
  description: """
  Yojson is an optimized parsing and printing library for the JSON format.
  
  It addresses a few shortcomings of json-wheel including 2x
  speedup,
  polymorphic variants and optional syntax for tuples and variants.
  
  ydump is a pretty-printing command-line program provided with the
  yojson package.
  
  The program atdgen can be used to derive OCaml-JSON serializers
  and
  deserializers from type definitions."""
  maintainer: "martin@mjambon.com"
  authors: "Martin Jambon"
  homepage: "https://github.com/ocaml-community/yojson"
  doc: "https://ocaml-community.github.io/yojson/"
  bug-reports: "https://github.com/ocaml-community/yojson/issues"
  depends: [
    "ocaml" {>= "4.02.3"}
    "dune"
    "cppo" {build}
    "easy-format"
    "biniou" {>= "1.2.0"}
    "alcotest" {with-test & >= "0.8.5"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  run-test: ["dune" "runtest" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocaml-community/yojson.git"
  url {
    src:
     
  "https://github.com/ocaml-community/yojson/releases/download/1.7.0/yojson-1.7.0.tbz"
    checksum: [
      "md5=b89d39ca3f8c532abe5f547ad3b8f84d"
     
  "sha256=656fc65f794186274f8b961dc38daba9e2de2fc993829291defbda2186812cc6"
     
  "sha512=01a8ef04e98bf0806905d6a7ee6e1e7ef51be557767fab6b06e7345d7e2deb7f7a0bc1531096ac3a7db6e65e12b0183a18052af8382ac54c368a2f8210b5e043"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cppo, easy-format, biniou, alcotest ? null, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.7.0"; in
assert (vcompare ocaml "4.02.3") >= 0;
assert (vcompare biniou "1.2.0") >= 0;
assert doCheck -> (vcompare alcotest "0.8.5") >= 0;

stdenv.mkDerivation rec {
  pname = "yojson";
  version = "1.7.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-community/yojson/releases/download/1.7.0/yojson-1.7.0.tbz";
    sha256 = "1iich6323npvvs8r50lkr4pxxqm9mf6w67cnid7jg1j1g5gwcvv5";
  };
  buildInputs = [
    ocaml dune cppo easy-format biniou alcotest findlib ];
  propagatedBuildInputs = [
    ocaml dune easy-format biniou alcotest ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
