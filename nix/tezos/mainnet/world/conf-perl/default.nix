/*opam-version: "2.0"
  name: "conf-perl"
  version: "1"
  synopsis: "Virtual package relying on perl"
  description:
    "This package can only install if the perl program is installed on the
  system."
  maintainer: "tim@gfxmonk.net"
  authors: "Larry Wall"
  license: "GPL-1.0-or-later"
  homepage: "https://www.perl.org/"
  bug-reports: "https://github.com/ocaml/opam-repository/issues"
  flags: conf
  build: ["perl" "--version"]
  depexts: [
    ["perl"] {os-family = "debian"}
    ["perl"] {os-distribution = "alpine"}
    ["perl"] {os-distribution = "nixos"}
    ["perl"] {os-distribution = "arch"}
    ["perl-Pod-Html"] {os-distribution = "fedora"}
  ]*/
{ runCommand, doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv,
  opam, fetchurl, findlib, perl }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1"; in

stdenv.mkDerivation rec {
  pname = "conf-perl";
  version = "1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    findlib perl ];
  propagatedBuildInputs = [
    perl ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'perl'" "'--version'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
