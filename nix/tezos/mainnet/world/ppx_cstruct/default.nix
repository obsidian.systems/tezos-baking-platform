/*opam-version: "2.0"
  name: "ppx_cstruct"
  version: "5.0.0"
  synopsis: "Access C-like structures directly from OCaml"
  description: """
  Cstruct is a library and syntax extension to make it easier to access
  C-like
  structures directly from OCaml.  It supports both reading and writing to
  these
  structures, and they are accessed via the `Bigarray` module."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Richard Mortier"
    "Thomas Gazagnaire"
    "Pierre Chambart"
    "David Kaloper"
    "Jeremy Yallop"
    "David Scott"
    "Mindy Preston"
    "Thomas Leonard"
    "Etienne Millon"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:ocamllabs"]
  homepage: "https://github.com/mirage/ocaml-cstruct"
  doc: "https://mirage.github.io/ocaml-cstruct/"
  bug-reports: "https://github.com/mirage/ocaml-cstruct/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {>= "1.0"}
    "cstruct" {= version}
    "ounit" {with-test}
    "ppx_tools_versioned" {>= "5.0.1"}
    "ocaml-migrate-parsetree"
    "ppx_sexp_conv" {with-test & < "v0.13"}
    "sexplib" {< "v0.13"}
    "cstruct-sexp" {with-test}
    "cstruct-unix" {with-test & = version}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs]
      {with-test & ocaml:version < "4.08.0"}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cstruct.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cstruct/releases/download/v5.0.0/cstruct-v5.0.0.tbz"
    checksum: [
     
  "sha256=eb8a4e4438ca4ab59e9d98ca70177edd8b590136fe7a200fe8e5bf69051e80fc"
     
  "sha512=414c2c780200252b5ebf16dd4fd1db28ffa483dba5be1c0092e08327d1d870f688c6f671892dcd8bbcf579f56e3d27b345ec0a96209fb25c0a984825b2e144f5"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cstruct, ounit ? null, ppx_tools_versioned,
  ocaml-migrate-parsetree, ppx_sexp_conv ? null, sexplib,
  cstruct-sexp ? null, cstruct-unix ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "5.0.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.0") >= 0;
assert stdenv.lib.getVersion cstruct == version;
assert (vcompare ppx_tools_versioned "5.0.1") >= 0;
assert doCheck -> (vcompare ppx_sexp_conv "v0.13") < 0;
assert (vcompare sexplib "v0.13") < 0;
assert doCheck -> stdenv.lib.getVersion cstruct-unix == version;

stdenv.mkDerivation rec {
  pname = "ppx_cstruct";
  version = "5.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cstruct/releases/download/v5.0.0/cstruct-v5.0.0.tbz";
    sha256 = "1z403q2nkgz5x07j0ypy6q0mk2yxgqbp1jlqkngbajna7124x2pb";
  };
  buildInputs = [
    ocaml dune cstruct ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_tools_versioned ocaml-migrate-parsetree ppx_sexp_conv sexplib ]
  ++
  stdenv.lib.optional
  doCheck
  cstruct-sexp
  ++
  [
    cstruct-unix findlib ];
  propagatedBuildInputs = [
    ocaml dune cstruct ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    ppx_tools_versioned ocaml-migrate-parsetree ppx_sexp_conv sexplib ]
  ++
  stdenv.lib.optional
  doCheck
  cstruct-sexp
  ++
  [
    cstruct-unix ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    (doCheck && (vcompare (stdenv.lib.getVersion ocaml) "4.08.0") < 0) [
      "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ])
    ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
