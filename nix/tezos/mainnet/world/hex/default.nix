/*opam-version: "2.0"
  name: "hex"
  version: "1.4.0"
  synopsis: "Library providing hexadecimal converters"
  description: """
  ```ocaml
  #require "hex";;
  # Hex.of_string "Hello world!";;
  - : Hex.t = "48656c6c6f20776f726c6421"
  # Hex.to_string "dead-beef";;
  - : string = "ޭ��"
  # Hex.hexdump (Hex.of_string "Hello world!
  ")
  00000000: 4865 6c6c 6f20 776f 726c 6421 0a        Hello world!.
  - : unit = ()
  ```"""
  maintainer: "thomas@gazagnaire.org"
  authors: ["Thomas Gazagnaire" "Trevor Summers Smith"]
  license: "ISC"
  homepage: "https://github.com/mirage/ocaml-hex"
  doc: "https://mirage.github.io/ocaml-hex/"
  bug-reports: "https://github.com/mirage/ocaml-hex/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune" {>= "1.0"}
    "cstruct" {>= "1.7.0"}
    "bigarray-compat" {>= "1.0.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-hex.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-hex/releases/download/v1.4.0/hex-v1.4.0.tbz"
    checksum: [
      "md5=57103ff33e70f14171c46d88f5452d11"
     
  "sha256=57e3492fa085eb332d7cd84f2ca1701a5940e4a4b19b9a27d59a3a5b29f0691d"
     
  "sha512=51fe702a5ffdb53380c4528c28f84d814a3c2c4cdfc1cc097348816fc66f27a0c871a2600549a2b6c7f1dff760386f7687de3a111343e526eb9bd86f92ef1814"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cstruct, bigarray-compat, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.4.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert (vcompare dune "1.0") >= 0;
assert (vcompare cstruct "1.7.0") >= 0;
assert (vcompare bigarray-compat "1.0.0") >= 0;

stdenv.mkDerivation rec {
  pname = "hex";
  version = "1.4.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-hex/releases/download/v1.4.0/hex-v1.4.0.tbz";
    sha256 = "07b9y0lmnflsslkrm6xilkj40n8sf2hjqkyqghnk7sw5l0plkqsp";
  };
  buildInputs = [
    ocaml dune cstruct bigarray-compat findlib ];
  propagatedBuildInputs = [
    ocaml dune cstruct bigarray-compat ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
