/*opam-version: "2.0"
  name: "ppxlib"
  version: "0.8.0"
  synopsis: "Base library and tools for ppx rewriters"
  description: """
  A comprehensive toolbox for ppx development. It features:
  - a OCaml AST / parser / pretty-printer snapshot,to create a full
     frontend independent of the version of OCaml;
  - a library for library for ppx rewriters in general, and type-driven
    code generators in particular;
  - a feature-full driver for OCaml AST transformers;
  - a quotation mechanism allowing  to write values representing the
     OCaml AST in the OCaml syntax;
  - a generator of open recursion classes from type
  definitions."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml-ppx/ppxlib"
  doc: "https://ocaml-ppx.github.io/ppxlib/"
  bug-reports: "https://github.com/ocaml-ppx/ppxlib/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "base" {>= "v0.11.0" & < "v0.13"}
    "dune" {build}
    "ocaml-compiler-libs" {>= "v0.11.0"}
    "ocaml-migrate-parsetree" {>= "1.3.1"}
    "ppx_derivers" {>= "1.0"}
    "stdio" {>= "v0.11.0" & < "v0.13"}
    "ocamlfind" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  run-test: ["dune" "runtest" "-p" name "-j" jobs] {ocaml >= "4.06"}
  dev-repo: "git+https://github.com/ocaml-ppx/ppxlib.git"
  url {
    src: "https://github.com/ocaml-ppx/ppxlib/archive/0.8.0.tar.gz"
    checksum: [
      "md5=9b1cfe1ae58d7ad851ed5618c1bf64a0"
     
  "sha256=22c3989affe54df7681603d76b26edb923f5d02bec5b3959729dd133e9fc79e2"
     
  "sha512=ac3cd84030de61d0f875512c6eb4c76d8456518047ad86fafef1fcfb587a7f987bf25d610d40274768ef550238ba151bac8e72cd42a6b3cf35642ddaed417274"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, ocaml-compiler-libs, ocaml-migrate-parsetree,
  ppx_derivers, stdio, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.0"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare base "v0.11.0") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare ocaml-compiler-libs "v0.11.0") >= 0;
assert (vcompare ocaml-migrate-parsetree "1.3.1") >= 0;
assert (vcompare ppx_derivers "1.0") >= 0;
assert (vcompare stdio "v0.11.0") >= 0 && (vcompare stdio "v0.13") < 0;

stdenv.mkDerivation rec {
  pname = "ppxlib";
  version = "0.8.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppxlib/archive/0.8.0.tar.gz";
    sha256 = "1qkrzklk7lcxf9ckjnzc5g8ga8xrxlk6pmq32rlgfkg5zyd9ihr2";
  };
  buildInputs = [
    ocaml base dune ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers
    stdio findlib ];
  propagatedBuildInputs = [
    ocaml base ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers stdio ]
  ++
  stdenv.lib.optional
  doCheck
  findlib;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
