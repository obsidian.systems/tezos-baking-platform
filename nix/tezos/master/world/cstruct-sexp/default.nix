/*opam-version: "2.0"
  name: "cstruct-sexp"
  version: "5.1.1"
  synopsis: "S-expression serialisers for C-like structures"
  description: """
  Cstruct is a library and syntax extension to make it easier to access
  C-like
  structures directly from OCaml.  It supports both reading and writing to
  these
  structures, and they are accessed via the `Bigarray` module.
  
  This library provides Sexplib serialisers for the Cstruct.t
  values."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Richard Mortier"
    "Thomas Gazagnaire"
    "Pierre Chambart"
    "David Kaloper"
    "Jeremy Yallop"
    "David Scott"
    "Mindy Preston"
    "Thomas Leonard"
    "Anton Kochkov"
    "Etienne Millon"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:ocamllabs"]
  homepage: "https://github.com/mirage/ocaml-cstruct"
  doc: "https://mirage.github.io/ocaml-cstruct/"
  bug-reports: "https://github.com/mirage/ocaml-cstruct/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "dune"
    "sexplib"
    "cstruct" {= version}
    "alcotest" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cstruct.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cstruct/releases/download/v5.1.1/cstruct-v5.1.1.tbz"
    checksum: [
     
  "sha256=55d1f42cb85f7872fee499c5ed382aea17b06d55d1709e071d1ba85c7a09fef3"
     
  "sha512=c3aa9a5a9125a1d022506a76fd7cdf32b21edcdc9df1202d8a9f382d02a28a33fea9a958f79e9302907ade1fce3f166b620c320aed6486e3efcc9a7464379cab"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, sexplib, cstruct, alcotest ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "5.1.1"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert stdenv.lib.getVersion cstruct == version;

stdenv.mkDerivation rec {
  pname = "cstruct-sexp";
  version = "5.1.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cstruct/releases/download/v5.1.1/cstruct-v5.1.1.tbz";
    sha256 = "1wzy15x5ra0v3l3rww6iamnv05za58wfvicrwkz74y2zp0ng9lam";
  };
  buildInputs = [
    ocaml dune sexplib cstruct ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml dune sexplib cstruct ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
