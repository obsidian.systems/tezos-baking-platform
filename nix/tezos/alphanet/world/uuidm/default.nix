/*opam-version: "2.0"
  name: "uuidm"
  version: "0.9.7"
  synopsis: "Universally unique identifiers (UUIDs) for OCaml"
  description: """
  Uuidm is an OCaml module implementing 128 bits universally
  unique
  identifiers version 3, 5 (named based with MD5, SHA-1 hashing) and 4
  (random based) according to [RFC 4122][rfc4122].
  
  Uuidm has no dependency and is distributed under the ISC license.
  
  [rfc4122]: http://tools.ietf.org/html/rfc4122"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  license: "ISC"
  tags: ["uuid" "codec" "org:erratique"]
  homepage: "https://erratique.ch/software/uuidm"
  doc: "https://erratique.ch/software/uuidm/doc/Uuidm"
  bug-reports: "https://github.com/dbuenzli/uuidm/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
  ]
  depopts: ["cmdliner"]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--pinned"
    "%{pinned}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/uuidm.git"
  url {
    src: "https://erratique.ch/software/uuidm/releases/uuidm-0.9.7.tbz"
    checksum: [
      "md5=54658248e3981d8c05237d0a4277ccd3"
     
  "sha256=2817b2034ac8b2f9f251f4ae903dd1a2d8ed48bfa6f758b3307325dbe1587dc7"
     
  "sha512=453bfd04d047e963e0a817de85aa2742dfa8ba09b506c9e1861c1139cf5e025564812c23cb19fa26be8998f80b4ebcfe2e4dd45b122f0534fe6239d9ea745646"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, cmdliner ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.9.7"; in
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "uuidm";
  version = "0.9.7";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/uuidm/releases/uuidm-0.9.7.tbz";
    sha256 = "1ivxb3hxn9bk62rmixx6px4fvn52s4yr1bpla7rgkcn8981v45r8";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ]
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  propagatedBuildInputs = [
    ocaml ]
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false"
      "'--with-cmdliner'" "${if cmdliner != null then "true" else "false"}" ] ];
    preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
    [ ];
    installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
    createFindlibDestdir = true;
  }
