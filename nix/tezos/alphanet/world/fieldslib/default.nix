/*opam-version: "2.0"
  name: "fieldslib"
  version: "v0.12.0"
  synopsis:
    "Syntax extension to define first class values representing record
  fields, to get and set record fields, iterate and fold over all fields of a
  record and create new record values"
  description: """
  Part of Jane Street's Core library
  The Core suite of libraries is an industrial strength alternative to
  OCaml's standard library that was developed by Jane Street, the
  largest industrial user of OCaml."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/janestreet/fieldslib"
  doc:
   
  "https://ocaml.janestreet.com/ocaml-core/latest/doc/fieldslib/index.html"
  bug-reports: "https://github.com/janestreet/fieldslib/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "base" {>= "v0.12" & < "v0.13"}
    "dune" {build & >= "1.5.1"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/janestreet/fieldslib.git"
  url {
    src:
     
  "https://ocaml.janestreet.com/ocaml-core/v0.12/files/fieldslib-v0.12.0.tar.gz"
    checksum: [
      "md5=7cb44f0fb396b6645fc9965ebb8e6741"
     
  "sha256=984b1c8bb8e5202d9a367a6856627210943080f7f1d63c0a2913ef64fa2f1683"
     
  "sha512=c103ea6d35213549e75c0cc4ade2828a5ff5bec56edb6a83ffbdcdeff57a65ea117c33432cd8e8b9cfdfe70d26b8d4e22b4717f401c50a4e8a1eb80c78d6b2b7"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "v0.12.0"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare base "v0.12") >= 0 && (vcompare base "v0.13") < 0;
assert (vcompare dune "1.5.1") >= 0;

stdenv.mkDerivation rec {
  pname = "fieldslib";
  version = "v0.12.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://ocaml.janestreet.com/ocaml-core/v0.12/files/fieldslib-v0.12.0.tar.gz";
    sha256 = "10qn5zx69vqk5453rmpiyy03150hf9i5cs3s6sd2s875p25iqjwq";
  };
  buildInputs = [
    ocaml base dune findlib ];
  propagatedBuildInputs = [
    ocaml base dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
